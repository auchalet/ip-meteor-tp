import { Template } from 'meteor/templating';

import '../imports/config/at_config.js';
import '../imports/config/routes.js';

import '../imports/ui/body.js';

BlazeLayout.setRoot('body')