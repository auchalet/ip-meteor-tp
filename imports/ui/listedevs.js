import { Devs } from '../api/devs.js'
import { Votes } from '../api/votes.js'
import { Session } from 'meteor/session'

let date = new Date();
let day = date.getDate() + '-' + date.getMonth() + '-' + date.getFullYear();

    
    
Template.listedevs.helpers({
  getDevs() {
    return Devs.find({}, {sort: {votes:-1}});
  },
  getDevsCount() {
    count = Devs.find().count();
    var rStr = "";
    if (count == 0) {
      rStr = "Aucun développeur inscrit";
    } else if (count == 1) {
      rStr = "Un seul développeur inscrit";
    } else {
      rStr = count + " développeurs inscrits";
    }
    return rStr;
  },
  getVotesCount() {
    var votesCount = 0
    Devs.find().map(function(dev) {
      votesCount += dev.votes
    })
    if (votesCount == 0) {
      rStr = "Aucun vote enregistré";
    } else if (votesCount == 1) {
      rStr = "Un seul vote enregistré";
    } else {
      rStr = votesCount + " votes enregistrés";
    }
    return rStr;
  },
  hasVotes(votes) {
    return votes > 0;
  },
  stateClass() {
    return Session.equals('currentDev', this._id) ? "success" : ""
  },
  deleteBtn() {
    return Session.equals('currentDev', this._id) ?
    '<button class="deleteBtn btn btn-danger"><span class="fa fa-trash"></span></button>' :
     '';
  },
  countVoteUser() {
      var count = Votes.find({user: Meteor.userId(), date: day}).count();
      console.log(count);
      var rest = 3-count;
      
      return count + " votes effectués aujourd'hui - "+rest+" restants";
  }

});

Template.listedevs.events({
  'click .devName' : function(e) {
    Session.set('currentDev', this._id);
    console.log(e.target);

    
  },

  'click button.deleteBtn' : function(e) {
    var devId = Session.get('currentDev');
    swal({
      title: "Confirmez-vous ?",
      text: "Ce développeur sera définitivement suppimé !",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-danger",
      confirmButtonText: "Oui, supprimer!",
      closeOnConfirm: false
    },
    function(){
      Devs.remove(devId);
      swal("Supprimé !", "Ce développeur a été supprimé", "success");

    });
  },

});

Template.voteDev.events({
  'click button.voteBtn' : function(e) {
      
    var count = Votes.find({user: Meteor.userId(), date: day}).count();
    if(count < 3) {
        var devId = Session.get('currentDev');
        Devs.find({_id: devId}).fetch();
        Devs.update(devId, {$inc: {votes: 1}});

        let authUser = Meteor.userId();

        let day = date.getDate();
        let month = date.getMonth();
        let year = date.getFullYear();

        let today = day + '-' + month + '-' + year
        Votes.insert({user: authUser, date: today});
    } else {
        swal("Erreur", "Vous ne pouvez plus voter aujourd'hui !", "error")
    }
      


  }
  
});

Template.formDev.events({
  'click button.addBtn' : function(e) {
      console.log('add');
    $('.addForm').removeClass('hidden');
  },
  'click button.cancelBtn' : function(e) {
    $('.addForm').addClass('hidden');
  },
  'submit form#newDevForm': function(e) {
    e.preventDefault();
    var devName = e.target.devNameInput.value;
    Devs.insert({name: devName, votes: 0});
    swal("Ajouté !", "Le développeur " + devName + " a été créé", "success");
    e.target.devNameInput.value = '';
    $('.addForm').addClass('hidden');
  }
})


