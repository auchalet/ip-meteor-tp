FlowRouter.route('/', {
    name: 'home',
    action: function() {
        
        console.log('index');
        BlazeLayout.render('layout1', {
            nav: 'navigation',
            main: 'index',
            footer: 'footer'
        });
    }   
});

FlowRouter.route('/vote', {
    name: 'vote',
    triggersEnter: [AccountsTemplates.ensureSignedIn],    
    action: function() {
        
        console.log('vote');
        BlazeLayout.render('layout1', {
            nav: 'navigation',
            main: 'vote',
            footer: 'footer'
            
        });
    }   
});

FlowRouter.route('/sign-out', {
    name: 'signOut',
    action: function() {
        AccountsTemplates.logout();
    }
});