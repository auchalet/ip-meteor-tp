AccountsTemplates.configure({
    showForgotPasswordLink: true,
    defaultLayout: 'layout1',
    defaultLayoutRegions: {
        nav: 'navigation',
        footer: {}
    },
    defaultContentRegion: 'main',
    defaultTemplate: 'auth'
})


AccountsTemplates.configureRoute('signIn');
AccountsTemplates.configureRoute('signUp');
AccountsTemplates.configureRoute('forgotPwd');